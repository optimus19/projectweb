<?php
header('Content-Type: application/json');

$conn = mysqli_connect('localhost', 'root','','pizza');

$sqlQuery = "SELECT food_name,food_id,sum(total_amount) as sum FROM order_details group by food_id order by sum DESC limit 5 ";

$result = mysqli_query($conn,$sqlQuery);

$data = array();
foreach ($result as $row) {
	$data[] = $row;
}

mysqli_close($conn);

echo json_encode($data);
?>