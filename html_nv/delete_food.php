<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 09/06/2019
 * Time: 10:57
 */

include "connect_db.php";
global $connection;

$food_id = $_GET['id']+0;
?>
<script>
    if (confirm('Bạn có muốn xóa món ăn này?')) {
        <?php
        $query = "Delete from food where food_id=$food_id";
        mysqli_query($connection, $query);

        ?>
        alert('Đã xóa món ăn thành công.');
        location.href='ds_menu.php';
    } else {
        alert('Xóa món ăn không thành công, vui lòng kiểm tra lại');
        location.href='ds_menu.php';
    }
</script>
