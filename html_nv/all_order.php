<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 16:04
 */
include "connect_db.php";
global $connection;
$get_order_query = "SELECT * from orders";
$result = mysqli_query($connection, $get_order_query);
$order = [];
while ($num = mysqli_fetch_assoc($result)) {
    $order[] = $num;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Danh sách order</title>
</head>
<body>
<div id="nav_position">
</div>
<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Danh sách order</h4>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Tên khách</th>
                                    <th>Ngày thanh toán</th>
                                    <th>Thành tiền</th>
                                    <th>Tiền khách trả</th>
                                    <th>Mã giảm giá</th>
                                    <th>Ghi chú</th>
                                    <th>Phương thức thanh toán</th>
                                    <th>Ngày tạo</th>
                                    <th>Trạng thái giao hàng</th>
                                    <th>Trạng thái</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($order as $item):
                                    $order_id = $item['id'] + 0;
                                    $cus_id = $item['cus_id'] + 0;
                                    $payment_date = $item['payment_date'];
                                    $amount = $item['amount'] + 0;
                                    $total_amount = $item['total_amount'] + 0;
                                    $promo_id = $item['promotion_id'];
                                    $note = $item['note'];
                                    $payment_id = $item['payment_method_id'] + 0;
                                    $create_date = $item['create_date'];
                                    $delivery_status = $item['delivery_status'] + 0;
                                    $status = $item['status'] + 0;
                                    $delivery_id = $item['delivery_id'];

                                    if($cus_id!=0){
                                        $cus_name_query = "SELECT * from customer where cus_id=$cus_id";
                                        $result = mysqli_query($connection, $cus_name_query);
                                        $cus_info = mysqli_fetch_assoc($result);
                                        $cus_name = $cus_info['name'];
                                    } else {
                                        $cus_name_query = "SELECT * from delivery where id=$delivery_id";
                                        $result = mysqli_query($connection, $cus_name_query);
                                        $cus_info = mysqli_fetch_assoc($result);
                                        $cus_name = $cus_info['name'];
                                    }

                                    $promo_query = "SELECT * from promotion where id=$promo_id";
                                    $result = mysqli_query($connection, $promo_query);
                                    $promo_info = mysqli_fetch_assoc($result);
                                    $promo_name = $promo_info['name'];

                                    if ($payment_id = 1) {
                                        $payment_method = 'Tiền mặt';
                                    } else {
                                        $payment_method = 'Thẻ';
                                    }

                                    $deli_status_query = "SELECT * from status where status_id=$delivery_status limit 1";
                                    $result = mysqli_query($connection, $deli_status_query);
                                    $delivery_info = mysqli_fetch_assoc($result);
                                    $delivery_name = $delivery_info['name'];

                                    $status_query = "SELECT * from status where status_id=$status limit 1";
                                    $result = mysqli_query($connection, $status_query);
                                    $status_info = mysqli_fetch_assoc($result);
                                    $status_name = $status_info['name'];
                                    ?>
                                    <tr>
                                        <td><?php echo $order_id ?></td>
                                        <td><?php echo $cus_name ?></td>
                                        <td><?php echo $payment_date ?></td>
                                        <td><?php echo $amount ?></td>
                                        <td><?php echo $total_amount ?></td>
                                        <td><?php echo $promo_name ?></td>
                                        <td><?php echo $note ?></td>
                                        <td><?php echo $payment_method ?></td>
                                        <td><?php echo $create_date ?></td>
                                        <td><?php echo $delivery_name ?></td>
                                        <td><?php echo $status_name ?></td>
                                        <td><?php echo "<a href='order_detail.php?order_id=$order_id'>Xem chi tiết"?> </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <button style="margin-right: 25px" type="button" class="btn btn-fill btn-info pull-right" name="chinhSua">
                                <a href="them_order.php" style='color: #FFFFFF;'>Thêm order</a>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
</script>
</body>

</html>
