<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 10:54
 */
include "connect_db.php";
global $connection;
$stt=$_GET["stt"];
$get_pasta_query = "SELECT * from food where type_id=4";
$pasta_result = mysqli_query($connection, $get_pasta_query);
$pasta = [];
if ($pasta_result) {
    while ($num = mysqli_fetch_assoc($pasta_result)) {
        $pasta[] = $num;
    }
}
?>
<div class="row">
    <div class="col-md-10">
        <div class="form-group">
            <select class="form-control" name="pasta[]">
                <?php foreach ($pasta as $item1):
                    $food_id = $item1['food_id'];
                    $food_name = $item1['food_name'];
                    echo "<option value=$food_id >$food_name</option>";
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <input name="pastaQuantity[]" min="0" type="number" class="form-control" value="0">
        </div>
    </div>

    <div class="col-md-1">
        <div class="form-group">
            <button type="button" class="btn btn-fill btn-danger pasta_remove" name="remove" id="<?php echo $stt?>">
                <a href="#" style='color: #FFFFFF;'>X</a>
            </button>
        </div>
    </div>
</div>
