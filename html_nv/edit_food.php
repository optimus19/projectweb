<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 29/05/2019
 * Time: 11:36
 */
include "connect_db.php";
global $connection;
$food_id = $_GET['id'];
$get_specific_food_query = "SELECT * from food where food_id='$food_id'";
$food_result = mysqli_query($connection, $get_specific_food_query);
$food = mysqli_fetch_assoc($food_result);
$food_name = $food['food_name'];
$food_price = $food['food_price'];
$description = $food['description'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <link href="../css/css_login_nv.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Sửa món</title>
</head>
<body>
<div id="nav_position">

</div>

<div class="main-panel">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Chỉnh sửa món</h4>
                    </div>
                    <div class="content">
                        <form action="edit_food_into_db.php" method="post">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="tenMon">Tên món</label>
                                        <?php echo "<input type='text' class='form-control' value='$food_name' name='tenMon'>" ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="giaMon">Giá</label>
                                        <?php echo "<input type='number' class='form-control' value='$food_price' name='giaMon'>" ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select class="form-control" name="status">
                                            <option value=1>Kích hoạt</option>
                                            <option value=0>Tạm dừng</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="chuThich">Chú thích</label>
                                        <?php echo "<textarea rows='5' class='form-control' name='chuThich'>$description</textarea>"?>
                                    </div>
                                </div>
                            </div>
                            <?php echo "<input name='food_id' type='text' value='$food_id' hidden>" ?>
                            <button type="submit" class="btn btn-fill btn-info pull-right" name="chinhSua">Cập nhật thông tin món</button>
                            <div class="clearfix"></div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
</script>
</html>
