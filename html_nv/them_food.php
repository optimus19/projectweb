<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 09/06/2019
 * Time: 11:27
 */
include "connect_db.php";
global $connection;

$get_type_query = "SELECT * from food_type";
$type_result = mysqli_query($connection, $get_type_query);
$type = [];
if ($type_result) {
    while ($num = mysqli_fetch_assoc($type_result)) {
        $type[] = $num;
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <link href="../css/css_login_nv.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Thêm món</title>
</head>
<body>
<div id="nav_position">

</div>

<div class="main-panel">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Thêm món</h4>
                    </div>
                    <div class="content">
                        <form action="them_food_into_db.php" method="post">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="tenMon">Tên món</label>
                                        <?php echo "<input type='text' class='form-control' value='' name='tenMon'>" ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="giaMon">Giá</label>
                                        <?php echo "<input type='number' class='form-control' value='' name='giaMon'>" ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="loai">Loại</label>
                                        <select class="form-control" name="loai">
                                            <?php foreach ($type as $item):
                                                $type_id = $item['type_id'];
                                                $type_name = $item['name'];
                                                echo "<option value=$type_id>$type_name</option>";
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="chuThich">Chú thích</label>
                                        <?php echo "<textarea rows='5' class='form-control' name='chuThich'></textarea>" ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-fill btn-info pull-right" name="addFood">Thêm món
                            </button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
</script>
</html>
