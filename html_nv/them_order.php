<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 09:00
 */
include "connect_db.php";
global $connection;
$get_pizza_query = "SELECT * from food where type_id=1";
$pizza_result = mysqli_query($connection, $get_pizza_query);
$pizza = [];
if ($pizza_result) {
    while ($num = mysqli_fetch_assoc($pizza_result)) {
        $pizza[] = $num;
    }
}

$get_drink_query = "SELECT * from food where type_id=2";
$drink_result = mysqli_query($connection, $get_drink_query);
$drink = [];
if ($drink_result) {
    while ($num = mysqli_fetch_assoc($drink_result)) {
        $drink[] = $num;
    }
}

$get_burgur_query = "SELECT * from food where type_id=3";
$burgur_result = mysqli_query($connection, $get_burgur_query);
$burger = [];
if ($burgur_result) {
    while ($num = mysqli_fetch_assoc($burgur_result)) {
        $burger[] = $num;
    }
}

$get_pasta_query = "SELECT * from food where type_id=4";
$pasta_result = mysqli_query($connection, $get_pasta_query);
$pasta = [];
if ($pasta_result) {
    while ($num = mysqli_fetch_assoc($pasta_result)) {
        $pasta[] = $num;
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Thêm order</title>
</head>

<body>
<div id="nav_position">
</div>
<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Thêm order</h4>
                        </div>
                        <div class="content">
                            <form action="them_order_into_db.php" method="post">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Chọn pizza</label>
                                            <select class="form-control" name="pizza[]">
                                                <?php foreach ($pizza as $item1):
                                                    $food_id = $item1['food_id'];
                                                    $food_name = $item1['food_name'];
                                                    echo "<option value=$food_id >$food_name</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input name="pizzaQuantity[]" min="0" type="number" class="form-control" value="0">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Size</label>
                                            <select class="form-control" name="pizzaSize[]">
                                                <option value="Regular">Regular</option>
                                                <option value="Large">Large</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="them_pizza0"></div>
                                <div class="row" id="divThemPizza">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-sm" id="btnThemPizza">Thêm
                                                pizza
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>Chọn nước</label>
                                            <select class="form-control" name="drink[]">
                                                <?php foreach ($drink as $item):
                                                    $food_id = $item['food_id'];
                                                    $food_name = $item['food_name'];
                                                    echo "<option value=$food_id >$food_name</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input name="drinkQuantity[]" min="0" type="number" class="form-control" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div id="them_drink0"></div>
                                <div class="row" id="divThemDrink">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-sm" id="btnThemNuoc">Thêm nước</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>Chọn bánh burgur</label>
                                            <select class="form-control" name="burgur[]">
                                                <?php foreach ($burger as $item):
                                                    $food_id = $item['food_id'];
                                                    $food_name = $item['food_name'];
                                                    echo "<option value=$food_id >$food_name</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input name="burgurQuantity[]" min="0" type="number" class="form-control" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div id="them_burgur0"></div>
                                <div class="row" id="divThemBurgur">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-sm" id="btnThemBurgur">Thêm burgur</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>Chọn pasta</label>
                                            <select class="form-control" name="pasta[]">
                                                <?php foreach ($pasta as $item):
                                                    $food_id = $item['food_id'];
                                                    $food_name = $item['food_name'];
                                                    echo "<option value=$food_id >$food_name</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input name="pastaQuantity[]" min="0" type="number" class="form-control" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div id="them_pasta0"></div>
                                <div class="row" id="divThemPasta">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-sm" id="btnThemPasta">Thêm pasta</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tên khách hàng</label>
                                            <input name="cusName" type="text" class="form-control" placeholder="Họ và tên">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Số điện thoại</label>
                                            <input name="phone" type="text" class="form-control" placeholder="SĐT">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Địa chỉ</label>
                                            <input name="address" type="text" class="form-control" placeholder="Địa chỉ">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Ghi chú</label>
                                            <textarea name="note" rows="5" class="form-control"
                                                      placeholder="Ghi chú của khách hàng"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Mã giảm giá</label>
                                            <input name="promo" type="text" class="form-control" placeholder="Mã giảm giá">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Phương thức thanh toán</label>
                                            <div class="form-check">
                                                <label class="form-check-label" for="radio1">
                                                    <input type="radio" class="form-check-input" id="radio1" name="payment"
                                                           value="1" checked>Tiền mặt
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label" for="radio2">
                                                    <input type="radio" class="form-check-input" id="radio2" name="payment"
                                                           value="2">Thẻ
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" name="themOrder" class="btn btn-info btn-fill pull-right">Xác Nhận</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var pizza_count = 0;
    var drink_count = 0;
    var burgur_count = 0;
    var pasta_count = 0;

    $("#btnThemPizza").click(function (e) {
        e.preventDefault();
        $('#them_pizza' + pizza_count).load("them_order_pizza_row.php?stt="+pizza_count);
        pizza_count++;
        $("#divThemPizza").before("<div id=\"them_pizza" + pizza_count + "\"></div>")
    });

    $("#btnThemNuoc").click(function (e) {
        e.preventDefault();
        $('#them_drink' + drink_count).load("them_order_drink_row.php?stt="+drink_count);
        drink_count++;
        $("#divThemDrink").before("<div id=\"them_drink" + drink_count + "\"></div>")
    });

    $("#btnThemBurgur").click(function (e) {
        e.preventDefault();
        $('#them_burgur' + burgur_count).load("them_order_burgur_row.php?stt="+burgur_count);
        burgur_count++;
        $("#divThemBurgur").before("<div id=\"them_burgur" + burgur_count + "\"></div>")
    });

    $("#btnThemPasta").click(function (e) {
        e.preventDefault();
        $('#them_pasta' + pasta_count).load("them_order_pasta_row.php?stt="+pasta_count);
        pasta_count++;
        $("#divThemPasta").before("<div id=\"them_pasta" + pasta_count + "\"></div>")
    });
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
    $(document).on('click', '.pizza_remove', function(){
        var button_id = $(this).attr("id");
        $('#them_pizza'+button_id+'').remove();
    });
    $(document).on('click', '.drink_remove', function(){
        var button_id = $(this).attr("id");
        $('#them_drink'+button_id+'').remove();
    });
    $(document).on('click', '.burgur_remove', function(){
        var button_id = $(this).attr("id");
        $('#them_burgur'+button_id+'').remove();
    });
    $(document).on('click', '.pasta_remove', function(){
        var button_id = $(this).attr("id");
        $('#them_pasta'+button_id+'').remove();
    });

</script>
</body>
</html>
