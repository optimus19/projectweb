<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 09:14
 */
include "connect_db.php";
global $connection;
if (isset($_POST['themOrder'])) {
    $cus_name = $_POST["cusName"];
    $phone = $_POST["phone"] + 0;
    $address = $_POST["address"];
    $note = $_POST["note"];
    $payment = $_POST["payment"] + 0;
    $date = date("Y-m-d h:i:s");
    $emp_id = $_SESSION['emp_id'] + 0;
    $promo_code = strtoupper($_POST['promo']);
    $tongTien=0.0;
    $tongTienSauPromo=0.0;

    $promo_id_query="Select id from promotion where name ='$promo_code'";
    $promo_result = mysqli_query($connection, $promo_id_query);
    $promo=mysqli_fetch_assoc($promo_result);
    $promo_id=$promo['id']+0;

    $query = "INSERT INTO delivery (name, address, phone) VALUES ('$cus_name', '$address', '$phone')";
    mysqli_query($connection, $query);

    $emp_id = $_SESSION['emp_id'];

    $lastest_deli_query = "SELECT max(id) as id FROM delivery";
    $result = mysqli_query($connection, $lastest_deli_query);
    $del_id = mysqli_fetch_assoc($result);
    $deli_id = $del_id['id']+0;

    $query = "INSERT INTO orders (emp_id, promotion_id, note, payment_method_id, create_date, delivery_id, status)
  			  VALUES($emp_id, $promo_id, '$note', $payment, '$date', $deli_id, 5)";
    mysqli_query($connection, $query);

    $latest_order_id_query = "SELECT max(id) id FROM orders";
    $result = mysqli_query($connection, $latest_order_id_query);
    $order_id_result = mysqli_fetch_assoc($result);
    $latest_order_id = $order_id_result['id'];


    for ($i = 0; $i < count($_POST["pizza"]); $i++) {
        $pizza_quantity = $_POST["pizzaQuantity"][$i];
        if ($pizza_quantity > 0) {
            $pizza_id = $_POST["pizza"][$i];

            $pizza_size = $_POST["pizzaSize"][$i];

            $get_pizza_query = "SELECT * from food where food_id=$pizza_id";
            $pizza_result = mysqli_query($connection, $get_pizza_query);
            $food = mysqli_fetch_assoc($pizza_result);
            $price = $food['food_price'];
            $food_name = $food['food_name'];
            $total_amount = $price * $pizza_quantity;

            $query = "insert into order_details(order_id, food_id, food_size, food_price, food_name, quantity, total_amount)
                  values ($latest_order_id, $pizza_id, '$pizza_size', '$price', '$food_name', $pizza_quantity, $total_amount)";
            $query_result = mysqli_query($connection, $query);
            $tongTien+=$total_amount;
        }
    }

    for ($i = 0; $i < count($_POST["burgur"]); $i++) {
        $burgur_quantity = $_POST["burgurQuantity"][$i];
        if ($burgur_quantity > 0) {
            $burgur_id = $_POST["burgur"][$i];

            $get_burgur_query = "SELECT * from food where food_id=$burgur_id";
            $burgur_result = mysqli_query($connection, $get_burgur_query);
            $food = mysqli_fetch_assoc($burgur_result);
            $price = $food['food_price'];
            $food_name = $food['food_name'];
            $total_amount = $price * $burgur_quantity;

            $query = "insert into order_details(order_id, food_id, food_price, food_name, quantity, total_amount)
                  values ($latest_order_id, $burgur_id, $price, '$food_name', $burgur_quantity, $total_amount)";
            $query_result = mysqli_query($connection, $query);
            $tongTien+=$total_amount;
        }
    }

    for ($i = 0; $i < count($_POST["drink"]); $i++) {
        $drink_quantity = $_POST["drinkQuantity"][$i];
        if ($drink_quantity > 0) {
            $drink_id = $_POST["drink"][$i];

            $get_drink_query = "SELECT * from food where food_id=$drink_id";
            $drink_result = mysqli_query($connection, $get_drink_query);
            $food = mysqli_fetch_assoc($drink_result);
            $price = $food['food_price'];
            $food_name = $food['food_name'];
            $total_amount = $price * $drink_quantity;

            $query = "insert into order_details(order_id, food_id, food_price, food_name, quantity, total_amount)
                  values ($latest_order_id, $drink_id, $price, '$food_name', $drink_quantity, $total_amount)";
            $query_result = mysqli_query($connection, $query);
            $tongTien+=$total_amount;
        }
    }

    for ($i = 0; $i < count($_POST["pasta"]); $i++) {
        $pasta_quantity = $_POST["pastaQuantity"][$i];
        if ($pasta_quantity > 0) {
            $pasta_id = $_POST["pasta"][$i];

            $get_pasta_query = "SELECT * from food where food_id=$pasta_id";
            $pasta_result = mysqli_query($connection, $get_pasta_query);
            $food = mysqli_fetch_assoc($pasta_result);
            $price = $food['food_price'];
            $food_name = $food['food_name'];
            $total_amount = $price * $pasta_quantity;

            $query = "insert into order_details(order_id, food_id, food_price, food_name, quantity, total_amount)
                  values ($latest_order_id, $pasta_id, $price, $food_name, '$pasta_quantity', $total_amount)";
            $query_result = mysqli_query($connection, $query);
            $tongTien+=$total_amount;
        }
    }

    $query="select percentage from promotion where id=$promo_id";
    $result=mysqli_query($connection,$query);
    $percent_result=mysqli_fetch_assoc($result);
    $percentage=$percent_result['percentage'];
    $tongTienSauPromo=(1-$percentage)*$tongTien;

    $query = "Update orders set amount=$tongTien, total_amount=$tongTienSauPromo where id=$latest_order_id";
    mysqli_query($connection, $query);

    echo "<script>alert('Đã đặt món thành công.');
location.href='all_order.php'
</script>";
} else {
    echo "<script>alert('Đặt món không thành công, vui lòng kiểm tra lại');location.href='all_order.php'</script>";
}