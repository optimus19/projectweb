<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 09:20
 */
include "connect_db.php";
global $connection;

$username = "";
$pass = "";
$errors = array();
$_SESSION['username'] = '';
$_SESSION['password'] = '';
$_SESSION['emp_id'] = '';

if (isset($_POST['submit'])) {
    $username = mysqli_real_escape_string($connection, $_POST['user']);
    $pass = mysqli_real_escape_string($connection, $_POST['pass']);

//kiem tra null
    if (empty($username)) {
        array_push($errors, "Email is required");
    }
    if (empty($pass)) {
        array_push($errors, "Password is required");
    }

//kiem tra thong tin co san hay chua
    $user_check_query = "SELECT username, password, emp_id FROM employee WHERE username='$username' and password = $pass LIMIT 1";
    $result = mysqli_query($connection, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    if ($user) { // if user exists
        if ($user['username'] === $username and $user['password'] === $pass) {
            header('location: ds_menu.php');
        } else array_push($errors, "email hoặc mật khẩu không đúng");
    }
    $_SESSION['username'] = $username;
    $_SESSION['password'] = $pass;
    $_SESSION['emp_id'] = $user['emp_id'];
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../css/html_nv_login.css">
    <title>Login</title>
</head>
<body class="loginpage">
<form class ="box" action="login.php" method="post">
    <h1>Login</h1>
    <input type="text" name="user" placeholder="Username">
    <input type="password" name="pass" placeholder="Password">
    <input type="submit" name="submit" value="Login">
</form>
</body>
</html>

