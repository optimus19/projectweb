<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 18:30
 */
include "connect_db.php";
global $connection;
$get_promo_query = "SELECT * from promotion";
$result = mysqli_query($connection, $get_promo_query);
$promo = [];
while ($num = mysqli_fetch_assoc($result)) {
    $promo[] = $num;
}
$stt=1;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Quản lý khuyến mãi</title>
</head>
<body>
<div id="nav_position">
</div>
<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Danh sách khuyến mãi</h4>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Mã khuyến mãi</th>
                                    <th>Phần trăm giảm giá</th>
                                    <th>Mô tả</th>
                                    <th>Ngày bắt đầu</th>
                                    <th>Ngày kết thúc</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($promo as $item):
                                    $promo_id = $item['id'] + 0;
                                    $name = $item['name'];
                                    $percentage = ($item['percentage']*100)."%";
                                    $description = $item['description'];
                                    $start_date = $item['start_date'];
                                    $end_date = $item['end_date'];
                                    $start_no_time = DateTime::createFromFormat('Y-m-d h:i:s', $start_date)->format('d-m-Y');
                                    $end_no_time = DateTime::createFromFormat('Y-m-d h:i:s', $end_date)->format('d-m-Y');
                                    ?>
                                    <tr>
                                        <td><?php echo $stt;$stt++ ?></td>
                                        <td><?php echo $name ?></td>
                                        <td><?php echo $percentage ?></td>
                                        <td><?php echo $description ?></td>
                                        <td><?php echo $start_no_time ?></td>
                                        <td><?php echo $end_no_time ?></td>
                                        <td><?php echo "<a href='edit_promotion.php?promo_id=$promo_id'>Chỉnh sửa"?> </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <button style="margin-right: 25px" type="button" class="btn btn-fill btn-info pull-right" name="chinhSua">
                                <a href="them_promotion.php" style='color: #FFFFFF;'>Thêm khuyến mãi</a>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
</script>
</body>

</html>
