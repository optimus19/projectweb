<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 18:09
 */
include "connect_db.php";
global $connection;
$order_id = $_GET['order_id']+0;

$get_order_detail_query = "SELECT * from order_details where order_id=$order_id";
$result = mysqli_query($connection, $get_order_detail_query);
$detail = [];
while ($num = mysqli_fetch_assoc($result)) {
    $detail[] = $num;
}
$stt=1;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Chi tiết order</title>
</head>
<body>
<div id="nav_position">
</div>
<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Chi tiết order <?php echo $order_id ?> </h4>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên món</th>
                                    <th>Kích cỡ</th>
                                    <th>Giá tiền</th>
                                    <th>Số lượng</th>
                                    <th>Tổng tiền</th>
                                    <th>Ghi chú</th>
                                    <th>Trạng thái</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($detail as $item):
                                    $food_size = $item['food_size'];
                                    $food_price = $item['food_price'];
                                    $food_name = $item['food_name'];
                                    $quantity = $item['quantity']+0;
                                    $total_amount = $item['total_amount'];
                                    $note = $item['note'];
                                    $status = $item['status'] + 0;

                                    $status_query = "SELECT * from status where status_id=$status limit 1";
                                    $result = mysqli_query($connection, $status_query);
                                    $status_info = mysqli_fetch_assoc($result);
                                    $status_name = $status_info['name'];
                                    ?>
                                    <tr>
                                        <td><?php echo $stt; $stt++ ?></td>
                                        <td><?php echo $food_name ?></td>
                                        <td><?php echo $food_size ?></td>
                                        <td><?php echo $food_price ?></td>
                                        <td><?php echo $quantity ?></td>
                                        <td><?php echo $total_amount ?></td>
                                        <td><?php echo $note ?></td>
                                        <td><?php echo $status_name ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
</script>
</body>

</html>

