<?php
include "connect_db.php";
global $connection;
$get_pizza_query = "SELECT * from food where type_id=1";
$pizza_result = mysqli_query($connection, $get_pizza_query);

$get_drink_query = "SELECT * from food where type_id=2";
$drink_result = mysqli_query($connection, $get_drink_query);

$get_burgur_query = "SELECT * from food where type_id=3";
$burgur_result = mysqli_query($connection, $get_burgur_query);

$get_pasta_query = "SELECT * from food where type_id=4";
$pasta_result = mysqli_query($connection, $get_pasta_query);

//$fav_food_query = "SELECT food_id,COUNT(*) as count FROM order_details GROUP BY food_id ORDER BY count DESC limit 4;";
//$result = mysqli_query($connection, $fav_food_query);
//
//$fav_food = [];
//if ($result) {
//    while ($num = mysqli_fetch_assoc($result)) {
//        $fav_food[] = $num['food_id'];
//    }
//}
//foreach ($fav_food as $food_id) {
//    $food_query = "SELECT * from food where food_id=$food_id";
//    $result1 = mysqli_query($connection, $food_query);
//    $food = [];
//    if ($result1) {
//        while ($num = mysqli_fetch_assoc($result1)) {
//            $food[] = $num;
//        }
//    }
//}

/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 24/05/2019
 * Time: 15:41
 */

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="../trung/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <link href="../css/css_login_nv.css" rel="stylesheet"/>
    <script src="../js/jquery.min.js"></script>
    <title>Danh sách menu</title>
</head>

<body>


<!--start of navigation sidebar-->
<div id="nav_position">

</div>
<!--end of navigation sidebar-->

<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="title">Quản lý menu</h2>
                        </div>
                        <div class="content">
                            <div>
                                <a href="#pizza_menu">Pizza</a>
                                <a href="#burgur_menu">Burger</a>
                                <a href="#pasta_menu">Pasta</a>
                                <a href="#drink_menu">Drink</a>
                            </div>
                            <!--pizza menu-->
                            <div class="row" id="pizza_menu">
                                <div class="col-md-6">
                                    <h4>Pizza</h4>
                                </div>
                                <div class="col-md-6" align="right">
                                    <a href="them_food.php"><h4>Thêm</h4></a>
                                </div>
                            </div>
                            <div class="row row-eq-height">
                                <?php
                                while ($food = mysqli_fetch_assoc($pizza_result)) {
                                    $food_id = $food['food_id'];
                                    $food_name = $food['food_name'];
                                    $food_price_small = $food['food_price'];
                                    $food_price_big = $food_price_small + 100000;
                                    $avatar = $food['avatar'];
                                    $description = $food['description'];
                                    $status = $food['food_status']+0;
                                    if ($status==0){
                                        $food_name= '[Tạm dừng]'.$food_name;
                                    }
                                    ?>

                                    <div class="col-md-4 text-center">
                                        <img src="../images/<?php echo $avatar ?>" width="200px" height="200px" alt=""
                                             onerror="this.src='../images/no-img.png'">
                                        <div class="text menu-flex">
                                            <b><?php echo $food_name ?></b>
                                            <p class="description" align="justify">
                                                <?php echo $description ?>
                                                <br>Size lớn: <?php echo $food_price_big ?>
                                                đ<br>Size vừa: <?php echo $food_price_small ?>đ
                                            </p>

                                            <div class="content" style="position: absolute; bottom: 10px;width: 100%;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill bottom-left">
                                                            <?php echo "<a href='edit_food.php?id=$food_id' style='color: #FFFFFF;' '>Sửa thông tin</a> " ?>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill bottom-right">
                                                            <?php echo "<a href='delete_food.php?id=$food_id' style='color: #FFFFFF;' '>Xóa món</a> " ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                <?php } ?>
                            </div>

                            <!--burgur menu-->
                            <div class="row" id="burgur_menu">
                                <div class="col-md-6">
                                    <h4>Burger</h4>
                                </div>
                                <div class="col-md-6" align="right">
                                    <a href="them_food.php"><h4>Thêm</h4></a>
                                </div>
                            </div>
                            <div class="row row-eq-height">
                                <?php
                                while ($food = mysqli_fetch_assoc($burgur_result)) {
                                    $food_id = $food['food_id'];
                                    $food_name = $food['food_name'];
                                    $food_price_small = $food['food_price'];
                                    $avatar = $food['avatar'];
                                    $description = $food['description'];
                                    $status = $food['food_status']+0;
                                    if ($status==0){
                                        $food_name= '[Tạm dừng]'.$food_name;
                                    }
                                    ?>

                                    <div class="col-md-4 text-center">
                                        <img src="../images/<?php echo $avatar ?>" width="200px" height="200px" alt=""
                                             onerror="this.src='../images/no-img.png'">
                                        <div class="text menu-flex">
                                            <b><?php echo $food_name ?></b>
                                            <p class="description" align="justify">
                                                <?php echo $description ?>
                                                <br>Giá: <?php echo $food_price_small ?>đ
                                            </p>
                                            <div class="content" style="position: absolute; bottom: 10px;width: 100%;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill pull-right">
                                                            <?php echo "<a href='edit_food.php?id=$food_id' style='color: #FFFFFF;' '>Sửa thông tin</a> " ?>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill pull-right">
                                                            <?php echo "<a href='delete_food.php?id=$food_id' style='color: #FFFFFF;' '>Xóa món</a> " ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                <?php } ?>
                            </div>
                            <!--pasta menu-->
                            <div class="row" id="pasta_menu">
                                <div class="col-md-6">
                                    <h4>Pasta</h4>
                                </div>
                                <div class="col-md-6" align="right">
                                    <a href="them_food.php"><h4>Thêm</h4></a>
                                </div>
                            </div>
                            <div class="row row-eq-height">
                                <?php
                                while ($food = mysqli_fetch_assoc($pasta_result)) {
                                    $food_id = $food['food_id'];
                                    $food_name = $food['food_name'];
                                    $food_price_small = $food['food_price'];
                                    $avatar = $food['avatar'];
                                    $description = $food['description'];
                                    $status = $food['food_status']+0;
                                    if ($status==0){
                                        $food_name= '[Tạm dừng]'.$food_name;
                                    }
                                    ?>

                                    <div class="col-md-4 text-center">
                                        <img src="../images/<?php echo $avatar ?>" width="200px" height="200px" alt=""
                                             onerror="this.src='../images/no-img.png'">
                                        <div class="text menu-flex">
                                            <b><?php echo $food_name ?></b>
                                            <p class="description" align="justify">
                                                <?php echo $description ?>
                                                <br>Giá: <?php echo $food_price_small ?>đ
                                            </p>
                                            <div class="content" style="position: absolute; bottom: 10px;width: 100%;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill pull-right">
                                                            <?php echo "<a href='edit_food.php?id=$food_id' style='color: #FFFFFF;' '>Sửa thông tin</a> " ?>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill pull-right">
                                                            <?php echo "<a href='delete_food.php?id=$food_id' style='color: #FFFFFF;' '>Xóa món</a> " ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                <?php } ?>
                            </div>
                            <!--drink menu-->
                            <div class="row" id="drink_menu">
                                <div class="col-md-6">
                                    <h4>Drink</h4>
                                </div>
                                <div class="col-md-6" align="right">
                                    <a href="them_food.php"><h4>Thêm</h4></a>
                                </div>
                            </div>
                            <div class="row row-eq-height">
                                <?php
                                while ($food = mysqli_fetch_assoc($drink_result)) {
                                    $food_id = $food['food_id'];
                                    $food_name = $food['food_name'];
                                    $food_price_small = $food['food_price'];
                                    $avatar = $food['avatar'];
                                    $description = $food['description'];
                                    $status = $food['food_status']+0;
                                    if ($status==0){
                                        $food_name = '[Tạm dừng]'.$food_name;
                                    }
                                    ?>

                                    <div class="col-md-4 text-center">
                                        <img src="../images/<?php echo $avatar ?>" width="200px" height="200px" alt=""
                                             onerror="this.src='../images/no-img.png'">
                                        <div class="text">
                                            <b><?php echo $food_name ?></b>
                                            <p class="description" align="justify">
                                                <?php echo $description ?>
                                                <br>Giá: <?php echo $food_price_small ?>đ
                                            </p>
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill pull-right">
                                                            <?php echo "<a href='edit_food.php?id=$food_id' style='color: #FFFFFF;' '>Sửa thông tin</a> " ?>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info btn-fill pull-right">
                                                            <?php echo "<a href='delete_food.php?id=$food_id' style='color: #FFFFFF;' '>Xóa món</a> " ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
<script>
    $(function () {
        $('#nav_position').load('navigation_bar.html');
    });
</script>
</html>