<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 10:54
 */
include "connect_db.php";
global $connection;
$stt=$_GET["stt"];
$get_pizza_query = "SELECT * from food where type_id=1";
$pizza_result = mysqli_query($connection, $get_pizza_query);
$pizza = [];
if ($pizza_result) {
    while ($num = mysqli_fetch_assoc($pizza_result)) {
        $pizza[] = $num;
    }
}
?>
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <select class="form-control" name="pizza[]">
                <?php foreach ($pizza as $item1):
                    $food_id = $item1['food_id'];
                    $food_name = $item1['food_name'];
                    echo "<option value=$food_id >$food_name</option>";
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <input name="pizzaQuantity[]" min="0" type="number" class="form-control" value="0">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <select class="form-control" name="pizzaSize[]">
                <option value="Regular">Regular</option>
                <option value="Large">Large</option>
            </select>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <button type="button" class="btn btn-fill btn-danger pizza_remove" name="remove" id="<?php echo $stt?>">
                <a href="them_order.php" style='color: #FFFFFF;'>X</a>
            </button>
        </div>
    </div>
</div>
