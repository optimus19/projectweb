<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 08/06/2019
 * Time: 10:54
 */
include "connect_db.php";
global $connection;
$stt=$_GET["stt"];
$get_burgur_query = "SELECT * from food where type_id=3";
$burgur_result = mysqli_query($connection, $get_burgur_query);
$burgur = [];
if ($burgur_result) {
    while ($num = mysqli_fetch_assoc($burgur_result)) {
        $burgur[] = $num;
    }
}
?>
<div class="row">
    <div class="col-md-10">
        <div class="form-group">
            <select class="form-control" name="pasta[]">
                <?php foreach ($burgur as $item1):
                    $food_id = $item1['food_id'];
                    $food_name = $item1['food_name'];
                    echo "<option value=$food_id >$food_name</option>";
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <input name="burgurQuantity[]" min="0" type="number" class="form-control" value="0">
        </div>
    </div>

    <div class="col-md-1">
        <div class="form-group">
            <button type="button" class="btn btn-fill btn-danger burgur_remove" name="remove" id="<?php echo $stt?>">
                <a href="#" style='color: #FFFFFF;'>X</a>
            </button>
        </div>
    </div>
</div>
