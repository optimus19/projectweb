<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 23/05/2019
 * Time: 21:49
 */
//
//0 male
//1 fermale

$fullName = "";
$phoneNumber = "";
$birthday = "";
$gender = "";
$email = "";
$password = "";
$errors = array();

require '../html_kh/connect.php';

if (isset($_POST['regUser'])) {
    $fullName = mysqli_real_escape_string($connection, $_POST['first_name']);
    $phoneNumber = mysqli_real_escape_string($connection, $_POST['phone']);
    $birthday = strtotime(mysqli_real_escape_string($connection, $_POST['birthday']));
    $gender = (int)mysqli_real_escape_string($connection, $_POST['gender']);
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $password = mysqli_real_escape_string($connection, $_POST['pass']);

//kiem tra null
    if (empty($fullName)) {
        array_push($errors, "Username is required");
    }
    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

//kiem tra thong tin co san hay chua
    $user_check_query = "SELECT * FROM customer WHERE name='$fullName' OR email='$email' LIMIT 1";
    $result = mysqli_query($connection, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    if ($user) { // if user exists
        if ($user['name'] === $fullName) {
            array_push($errors, "Username already exists");
        }

        if ($user['email'] === $email) {
            array_push($errors, "email already exists");
        }
    }
    if (count($errors) == 0) {
//        $password = md5($password);//encrypt the password before saving in the database

        $query = "INSERT INTO customer (name, email, password, phone_number,gender,birthday,status) 
  			  VALUES('$fullName', '$email', '$password', '$phoneNumber','$gender','$birthday',1)";
        mysqli_query($connection, $query);
    }
    header('location: ../html_kh/login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/register.css">
    <title>Register - Pizza Delicous</title>
</head>
<body>
<div class="header-main">
    <div class="header-right">
        <div class="header-left-bottom">
            <form align="center" action="register.php" method="post" style="height: 400px">
                <h3>Registration Form</h3>
                <div class="row row-space">
                    <div class="col-2">
                        <div class="input-group">
                            <label class="label">Fullname</label>
                            <input class="input--style-4" type="text" name="first_name">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="input-group">
                            <label class="label">Birthday</label>
                            <input class="input--style-4" type="date" name="birthday" style="width: 195px">
                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="col-2">
                        <div class="input-group">
                            <label class="label">Phone Number</label>
                            <input class="input--style-4" type="text" name="phone">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="input-group">
                            <label class="label">Gender</label>
                            <div class="p-t-10">
                                <label class="radio-container m-r-90">Male
                                    <input type="radio" checked="checked" name="gender" value="0">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio-container" style="margin-right: 60px">Female
                                    <input type="radio" name="gender" value="1">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="col-2">
                        <div class="input-group">
                            <label class="label">Email</label>
                            <input class="input--style-4" type="email" name="email">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Password</label>
                                <input class="input--style-4" type="password" name="pass">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-t-15">
                    <input type="submit" name="regUser" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>

<!--Begin script-->
<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/js/bootstrap.min.js"></script>
<!--End script-->
</body>
</html>