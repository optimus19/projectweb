<?php
require '../html_kh/connect.php';

$food1_query = "SELECT * from food where type_id=1";
$result1 = mysqli_query($connection, $food1_query);
$food1 = [];
if ($result1) {
    while ($num = mysqli_fetch_assoc($result1)) {
        $food1[] = $num;
    }
}

$food2_query = "SELECT * from food where type_id=2";
$result2 = mysqli_query($connection, $food2_query);
$food2 = [];
if ($result2) {
    while ($num = mysqli_fetch_assoc($result2)) {
        $food2[] = $num;
    }
}

$food3_query = "SELECT * from food where type_id=3";
$result3 = mysqli_query($connection, $food3_query);
$food3 = [];
if ($result3) {
    while ($num = mysqli_fetch_assoc($result3)) {
        $food3[] = $num;
    }
}

$food4_query = "SELECT * from food where type_id=4";
$result4 = mysqli_query($connection, $food4_query);
$food4 = [];
if ($result4) {
    while ($num = mysqli_fetch_assoc($result4)) {
        $food4[] = $num;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="../css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/flaticon.css">

    <title>Pizza Delicous</title>
</head>
<body>
<!--Menu-->
<nav class="navbar navbar-expand-lg bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="about.php"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
            <small>- Delicous -</small>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
                <li class="nav-item active"><a href="index.php" class="nav-link">Our Menu</a></li>
                <li class="nav-item"><a href="promotions.php" class="nav-link">Promotions</a></li>
                <li class="nav-item"><a href="about.php" class="nav-link">Abour Us</a></li>
                <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="login.php" class="nav-link"><span class="fa fa-user-circle"></span></a>
                </li>
                <li class="nav-item"><a href="cart.php" class="nav-link"><span class="fa fa-shopping-cart"></span> Your
                        Cart [<?php echo $_SESSION['totalqty'] ?>]</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--End menu-->

<section class="home-slider">
    <div class="slider-item" style="background-image: url(../images/bg_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center">
                <div class="col-md-7 col-sm-12 text-center">
                    <h1 class="mb-3 mt-5 bread">Our Menu</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a class="color" href="index.php">Home</a></span><span>Menu</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Menu-->
<section class="ftco-menu">
    <div class="container-fluid">
        <div class="row d-md-flex">
            <div class="col-lg-12 ftco-animate p-md-5">
                <div class="row">
                    <div class="col-md-12 mb-5">
                        <div class="nav nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" role="tab"
                               aria-controls="v-pills-1" aria-selected="true" href="#v-pills-1">Pizza</a>
                            <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab"
                               aria-controls="v-pills-2" aria-selected="false">Drinks</a>
                            <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab"
                               aria-controls="v-pills-3" aria-selected="false">Burgers</a>
                            <a class="nav-link" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab"
                               aria-controls="v-pills-4" aria-selected="false">Pasta</a>
                        </div>
                    </div>
                    <div class="col-md-12 d-flex align-items-center">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel"
                                     aria-labelledby="v-pills-1-tab">
                                    <div class="row">
                                        <?php foreach ($food1 as $item): ?>
                                            <div class="col-md-4 text-center">
                                                <form action="addcart.php" method="post">
                                                <div class="menu-wrap">
                                                    <img class="menu-img img mb-4"
                                                         src="../images/<?php echo $item['avatar'] ?>">
                                                    <div class="text">
                                                        <h3><?php echo $item['food_name'] ?></h3>
                                                        <input type="text" value="<?php echo $item['food_id'] ?>" name='food_id' hidden placeholder="">
                                                        <input type="text" value="<?php echo $item['type_id'] ?>" name='type_id' hidden placeholder="">
                                                        <p class="description"
                                                           align="justify"><?php echo $item['description'] ?>
                                                            <br>Large: <?php echo number_format($item['food_price'] + 100000, 0) ?>
                                                            đ
                                                            <br>Regular: <?php echo number_format($item['food_price']) ?>
                                                            đ</p>
                                                        <span class="col-3">Size</span>
                                                        <select class="custom-select-sm" name="size">
                                                            <option value="Regular">Regular</option>
                                                            <option value="Large">Large</option>
                                                        </select>
                                                        <p><button type="submit" name="product" class='btn mt-3'>Add to cart</button></p>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                        <div class="tab-pane fade" id="v-pills-2" role="tabpanel"
                             aria-labelledby="v-pills-2-tab">
                            <div class="row">
                                <?php foreach ($food2 as $item): ?>
                                    <div class="col-md-4 text-center">
                                        <form action="addcart.php" method="post">
                                        <div class="menu-wrap">
                                            <img href="#" class="menu-img img mb-4"
                                                 src="../images/<?php echo $item['avatar'] ?>">
                                            <div class="text">
                                                <h3><?php echo $item['food_name'] ?></h3>
                                                <input type="text" value="<?php echo $item['food_id'] ?>" name='food_id' hidden placeholder="">
                                                <input type="text" value="<?php echo $item['type_id'] ?>" name='type_id' hidden placeholder="">
                                                <p class="description"
                                                   align="justify"><?php echo $item['description'] ?></p>
                                                <p class="price"><?php echo number_format($item['food_price']) ?>đ</p>
                                                <p><button type="submit" name="product" class='btn mt-3'>Add to cart</button></p>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-3" role="tabpanel"
                             aria-labelledby="v-pills-3-tab">
                            <div class="row">
                                <?php foreach ($food3 as $item): ?>
                                    <div class="col-md-4 text-center">
                                        <form action="addcart.php" method="post">
                                        <div class="menu-wrap">
                                            <img href="#" class="menu-img img mb-4"
                                                 src="../images/<?php echo $item['avatar'] ?>">
                                            <div class="text">
                                                <h3><?php echo $item['food_name'] ?></h3>
                                                <input type="text" value="<?php echo $item['food_id'] ?>" name='food_id' hidden placeholder="">
                                                <input type="text" value="<?php echo $item['type_id'] ?>" name='type_id' hidden placeholder="">
                                                <p class="description"
                                                   align="justify"><?php echo $item['description'] ?></p>
                                                <p class="price"><?php echo number_format($item['food_price']) ?>đ</p>
                                                <p><button type="submit" name="product" class='btn mt-3'>Add to cart</button></p>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-4" role="tabpanel"
                             aria-labelledby="v-pills-4-tab">
                            <div class="row">
                                <?php foreach ($food4 as $item): ?>
                                    <div class="col-md-4 text-center">
                                        <form action="addcart.php" method="post">
                                        <div class="menu-wrap">
                                            <img href="#" class="menu-img img mb-4"
                                                 src="../images/<?php echo $item['avatar'] ?>">
                                            <div class="text">
                                                <h3><?php echo $item['food_name'] ?></h3>
                                                <input type="text" value="<?php echo $item['food_id'] ?>" name='food_id' hidden placeholder="">
                                                <input type="text" value="<?php echo $item['type_id'] ?>" name='type_id' hidden placeholder="">
                                                <p class="description"
                                                   align="justify"><?php echo $item['description'] ?></p>
                                                <p class="price"><?php echo number_format($item['food_price']) ?>đ</p>
                                                <p><button type="submit" name="product" class='btn mt-3'>Add to cart</button></p>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!--End Menu-->

<!--Footer-->
<footer class="ftco-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="mb-4">
                    <h1 class="bread ftco-heading-1"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
                        <small>- Delicous -</small>
                    </h1>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li><a href="#"><span class="icon-twitter"></span></a></li>
                        <li><a href="#"><span class="icon-facebook"></span></a></li>
                        <li><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Shop with us</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">About us</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Terms & Conditions</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Privacy Policy</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">How it works</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">Delivery Area and Free</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Exchange & Refund Policy</a></li>
                        <li><a href="#" class="py-2 d-block color">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Customer Service</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span>
                            </li>
                            <li><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></li>
                            <li><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright &copy; Pizza - Delicious - &copy; All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
<!--End footer-->
<!--Begin script-->
<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/js/bootstrap.min.js"></script>
<!--End script-->

</body>
</html>