<?php
require '../html_kh/connect.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="../css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/flaticon.css">
    <title>Pizza Delicous</title>
</head>

<body>
    <!--Menu-->
    <nav class="navbar navbar-expand-lg bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="about.php"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br><small>-
                    Delicous -</small></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false">
                <span class="oi oi-menu"></span> Menu
            </button>
            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
                    <li class="nav-item"><a href="menu.php" class="nav-link">Our Menu</a></li>
                    <li class="nav-item"><a href="promotions.php" class="nav-link">Promotions</a></li>
                    <li class="nav-item active"><a href="about.php" class="nav-link">Abour Us</a></li>
                    <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                    <li class="nav-item"><a href="login.php" class="nav-link"><span
                                class="fa fa-user-circle"></span></a></li>
                    <li class="nav-item"><a href="cart.php" class="nav-link"><span
                                class="fa fa-shopping-cart"></span> Your Cart [<?php echo $_SESSION['totalqty'] ?>]</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--End menu-->

    <section class="home-slider">
        <div class="slider-item" style="background-image: url(../images/bg_3.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text justify-content-center align-items-center">
                    <div class="col-md-7 col-sm-12 text-center">
                        <h1 class="mb-3 mt-5 bread">About</h1>
                        <p class="breadcrumbs"><span class="mr-2"><a class="color"
                                    href="index.php">Home</a></span><span>About</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="about">
        <h2>About Pizza Delicious Online Store?</h2>
        <h4>Make The World a Happier Place with Excitement and Positive Energy</h4>
        <p>Pizza Delicious Online Store is the online shopping channel of Pizza Delicious, a Japanese Italian pizza
            restaurant located in District 1, Ho Chi Minh City. The restaurant has been opened since 2011 with the
            vision “To make the world smile for Peace”. Our ambition goes beyond serving great pizzas; we aim at
            generating joy and peace for all our diners from pleasant dining experience.</p>
        <p>Regarding our “Delivering Wow, Sharing Happiness” philosophy, we believe that to achieve “Happiness”, we must
            have hearts filled with gratitude that feels fulfilled with the present. However, silent gratitude is not
            much use to anyone. Indeed, gratitude is an action word. Pizza Delicious Online Store is a manifestation of
            our gratitude for our customers and the community that we are living in.</p>
        <p>On our journey accomplishing our mission “Delivering Wow, Sharing Happiness”, we realize that ingredients
            play a crucial part in producing quality and healthy food. However, we cannot commit with our customers
            about the quality of the food products available on the market as we hold no accountability in testing them.
            Therefore, the best solution we found was to produce our own food.</p>
        <p>Our self-produced food system was established since then. All of our main ingredients was created handmade
            from our cheeses, dressings to desserts. At first, the homemade ingredients were only served in our
            restaurant but understanding our customers’ concern with food safety, we then expanded our food distribution
            to some reputable restaurants and hotels too.</p>
        <p>Accordingly, Pizza Delicious Online Store was founded to provide fresh, environmentally-sustainable and
            delicious food straight from our own farm in pursuance of provoking better food life in Vietnam. All of our
            products are homemade under strict condition control to guarantee our customers with the safety and the
            quality of the food. Especially, our house-made cheeses, the most complimented products, are hand-crafted
            carefully by our skillful artisans to ensure the condition of the products.</p>
        <p>Through our Online Store, we want to express our sincere thankfulness for our beloved consumers by providing
            quality food that improve their well-being. By this act, we hope to generate more smiles on our customers’
            face and fill the world with peace.</p>
    </div>

    <hr width="90%" color="white" align="center" />

    <h2 style="margin: 50px 60px">Why choose us?</h2>

    <section class="ftco-about d-md-flex" style="background: none;">
        <div class="one-half img" style="background-image: url(../images/choose_about_1.jpg);"></div>
        <div class="one-half">
            <div class="heading-section">
                <h2 class="mb-4">Housemade Cheese</h2>
            </div>
            <div>
                <p style="font-size: 16px">We launched the cheese studio in 2011, around the same time as the opening of
                    our first restaurant. We initially launched it to only produce mozzarella cheese to be used at our
                    restaurant, however we now have over 20 cheese artisans producing 400 servings of 8 different kinds
                    of cheese daily. Our Japanese cheese artisan who has been holding the studio down since the
                    beginning works on product development and quality improvement, while paying close attention to
                    daily production and quality control.</p>
            </div>
        </div>
    </section>
    <section class="ftco-about d-md-flex" style="background: none;">
        <div class="one-half">
            <div class="heading-section">
                <h2 class="mb-4">Delivering Wow</h2>
            </div>
            <div>
                <p style="font-size: 16px">We believe “Wow” moments are the key to our customers’ heart. It is to
                    deliver excitement and making our customers’ heart jump of happiness. With this positivity, we hope
                    to be able to create uplifting energy for our customers. “Delivering Wow” is one of our most
                    important mission, we want our treasured customers to experience the best and experience the “Wow”
                    feeling every time they receive products from our delivery. If our customers smile, we are happy. If
                    our customers satisfy, we are successful.</p>
            </div>
        </div>
        <div class="one-half img" style="background-image: url(../images/choose_about_2.jpg);"></div>
    </section>
    <section class="ftco-about d-md-flex" style="background: none;">
        <div class="one-half img" style="background-image: url(../images/choose_about_3.jpg);"></div>
        <div class="one-half">
            <div class="heading-section">
                <h2 class="mb-4">Passion "For Peace"</h2>
            </div>
            <div>
                <p style="font-size: 16px">We dream of putting long-lasting smiles on our customers’ faces when they
                    experience our service. That is the reason why at Pizza 4P’s, our aim does not stop at simply
                    delivering great service, we genuinely care about our customers’ happiness after using our service.
                    This vision follows us into our daily work and has been acted as a lodestar. We believe that the
                    first step to get closer to our vision is to spread love and put love into everything we do. What we
                    deliver through our products is not only genuine and real food, but also our aspiration to “Change
                    The World with Positive Energy, for Peace”. On the journey to achieve our vision “Change the World
                    with Positive Energy, for Peace”, we have figured out our foremost mission is “Delivering Wow,
                    Sharing Happiness”.</p>
            </div>
        </div>
    </section>
    <section class="ftco-about d-md-flex" style="background: none;">
        <div class="one-half">
            <div class="heading-section">
                <h2 class="mb-4">Omotenashi</h2>
            </div>
            <div>
                <p style="font-size: 16px">In the Japanese language, there is a word, “Omotenashi”. It means to welcome
                    guests from the heart, and this word represents the spirit of Japanese hospitality. "Omotenashi"
                    represents a considerate and compassionate mindset that sees hosting as much more than a mere task
                </p>
            </div>
        </div>
        <div class="one-half img" style="background-image: url(../images/choose_about_4.jpg);"></div>
    </section>
    <!--Footer-->
    <footer class="ftco-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="mb-4">
                        <h1 class="bread ftco-heading-1"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br><small>- Delicous -</small></h1>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <li><a href="#"><span class="icon-twitter"></span></a></li>
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <li><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Shop with us</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block color">About us</span></a></li>
                            <li><a href="#" class="py-2 d-block color">Terms & Conditions</span></a></li>
                            <li><a href="#" class="py-2 d-block color">Privacy Policy</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="ftco-footer-widget mb-4 ml-md-4">
                        <h2 class="ftco-heading-2">How it works</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block color">Delivery Area and Free</span></a></li>
                            <li><a href="#" class="py-2 d-block color">Exchange & Refund Policy</a></li>
                            <li><a href="#" class="py-2 d-block color">FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Customer Service</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                                <li><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></li>
                                <li><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Copyright &copy; Pizza - Delicious - &copy; All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>
    <!--End footer-->
    <!--Begin script-->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/js/bootstrap.min.js"></script>
    <!--End script-->
</body>

</html>