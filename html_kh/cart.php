<?php
require '../html_kh/connect.php';

$sum = 0;

if (! isset($_SESSION['cart']) || count($_SESSION['cart']) == 0)
{
    echo "<script>alert(' Không có sản phẩm trong giỏ hàng');location.href='index.php'</script>";
}

if (!isset($_SESSION['email']) && !isset($_SESSION['password']))
{
    echo "<script>alert('Chưa đăng nhập, yêu cầu đăng nhập');
    location.href='login.php'</script>";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="../css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/flaticon.css">
    <title>Pizza Delicous</title>
</head>

<body>
<!--Menu-->
<nav class="navbar navbar-expand-lg bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="about.php"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
            <small>-
                Delicous -
            </small>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="menu.php" class="nav-link">Our Menu</a></li>
                <li class="nav-item"><a href="promotions.php" class="nav-link">Promotion</a></li>
                <li class="nav-item"><a href="about.php" class="nav-link">Abour Us</a></li>
                <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="login.php" class="nav-link"><span
                        class="fa fa-user-circle"></span></a></li>
                <li class="nav-item active"><a href="cart.php" class="nav-link"><span
                        class="fa fa-shopping-cart"></span> Your Cart [<?php echo $_SESSION['totalqty'] ?>]</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--End menu-->

<section class="home-slider">
    <div class="slider-item" style="background-image: url(../images/bg_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center">
                <div class="col-md-7 col-sm-12 text-center">
                    <h1 class="mb-3 mt-5 bread">Your Cart</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a class="color" href="index.php">Home</a></span><span>Your cart</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cart-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="cart-table">
                    <h3>Your Cart</h3>
                        <table>
                            <thead>
                            <tr>
                                <th class="product-th">Product</th>
                                <th class="quy-th">Quantity</th>
                                <th class="size-th">Size</th>
                                <th class="total-th">Price</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($_SESSION['cart'] as $item): ?>
                            <tr>
                                <td class="product-col">
                                    <img src="../images/<?php echo $item['avatar'] ?>">
                                    <div class="pc-title">
                                        <h4><?php echo $item['food_name'] ?></h4>
                                        <p><?php echo $item['description'] ?></p>
                                    </div>
                                </td>
                                <td class="quy-col">
                                    <div class="btn-group" role="group" aria-label="Second group">
                                        <?php
                                        echo "<a class='btn mi-button' href='minus.php?id=".$item['food_id']."'>-</a>"
                                        ?>
                                        <input type="text" value="<?php echo $item['qty'] ?>" class="in-group" readonly="true">
                                        <?php
                                        echo "<a class='btn mi-button' href='plus.php?id=".$item['food_id']."'>+</a>"
                                        ?>
                                    </div>
                                </td>
                                <td class="size-col"><h4><?php echo $item['size'] ?></h4></td>
                                <td class="total-col"><h4><?php echo number_format($item['food_price']) ?>đ</h4></td>
                                <?php
                                echo "<td class='delete'><a href='deletecart.php?id=".$item['food_id']."'><span class='fa fa-minus'></span></a></td>"
                                ?>
                            </tr>
                                <?php $sum += $item['food_price'] * $item['qty']; $_SESSION['tongtien'] = $sum; ?>
                            <?php endforeach ?>
                            </tbody>
                        </table>

                    <div class="total-cost">
                        <div class="row">
                            <div class="col-md-9"><h6>Provisional</h6></div>
                            <div class="col-md-3"><span><?php echo number_format($_SESSION['tongtien']) ?> đ</span></div>
                        </div>
                        <div class="row">
                        <div class="col-md-9"><h6>Discount</h6></div>
                        <div class="col-md-3"><span><?php echo number_format($_SESSION['promotion']) ?> đ</span></div></div>
                        <hr>
                        <div class="row">
                            <div class="col-md-9"><h6>Total</h6></div>
                            <div class="col-md-3"><span><?php $_SESSION['total'] = $_SESSION['tongtien'] - $_SESSION['promotion']; echo number_format($_SESSION['total']); ?> đ</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 card-right">
                <form action="promotion.php" method="post" class="promo-code-form">
                    <input name="code" type="text" placeholder="Enter promo code">
                    <button name="promo-code" type="submit">Submit</button>
                </form>
                <a href="deliveryinfo.php" class="site-btn">Proceed to checkout</a>
                <a href="menu.php" class="site-btn sb-dark">Continue shopping</a>
            </div>
        </div>
    </div>
</section>


<!--Footer-->
<footer class="ftco-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="mb-4">
                    <h1 class="bread ftco-heading-1"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
                        <small>-
                            Delicous -
                        </small>
                    </h1>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li><a href="#"><span class="icon-twitter"></span></a></li>
                        <li><a href="#"><span class="icon-facebook"></span></a></li>
                        <li><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Shop with us</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">About us</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Terms & Conditions</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Privacy Policy</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">How it works</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">Delivery Area and Free</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Exchange & Refund Policy</a></li>
                        <li><a href="#" class="py-2 d-block color">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Customer Service</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain
                                        View, San Francisco, California, USA</span></li>
                            <li><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></li>
                            <li><span class="icon icon-envelope"></span><span
                                    class="text">info@yourdomain.com</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright &copy; Pizza - Delicious - &copy; All rights reserved.</p>
            </div>
        </div>
    </div>

</footer>
<!--End footer-->
<!--Begin script-->
<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/js/bootstrap.min.js"></script>
<!--End script-->
</body>
</html>
