<?php
require '../html_kh/connect.php';
$fav_food_query = "SELECT food_id,COUNT(*) as count FROM order_details GROUP BY food_id ORDER BY count DESC limit 4;";
$result = mysqli_query($connection, $fav_food_query);

$fav_food = [];
if ($result) {
    while ($num = mysqli_fetch_assoc($result)) {
        $fav_food[] = $num['food_id'];
    }
}

$food = [];
foreach ($fav_food as $food_id) {
    $food_query = "SELECT * from food where food_id=$food_id";
    $result1 = mysqli_query($connection, $food_query);

    if ($result1) {
        while ($num = mysqli_fetch_assoc($result1)) {
            $food[] = $num;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="../css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/flaticon.css">
    <link rel="stylesheet" href="../css/multi_carousel.css">

    <title>Pizza Delicous</title>
</head>

<body>
<!--Menu-->
<nav class="navbar navbar-expand-lg bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="about.php"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
            <small>-
                Delicous -
            </small>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="index.php" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="menu.php" class="nav-link">Our Menu</a></li>
                <li class="nav-item"><a href="promotions.php" class="nav-link">Promotions</a></li>
                <li class="nav-item"><a href="about.php" class="nav-link">Abour Us</a></li>
                <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="login.php" class="nav-link"><span
                                class="fa fa-user-circle"></span></a></li>
                <li class="nav-item"><a href="cart.php" class="nav-link"><span class="fa fa-shopping-cart"></span>
                        Your Cart [<?php echo $_SESSION['totalqty'] ?>]</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--End menu-->

<div>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100"
                     src="http://theme.hstatic.net/1000163589/1000464980/14/slideshow_3.jpg?v=38" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"
                     src="http://theme.hstatic.net/1000163589/1000464980/14/slideshow_2.jpg?v=38" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"
                     src="../images/catalog_1.png" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<h3 class="text-center mt-4 mb-4">The Favorite Food</h3>
<div class="container">
    <div class="row">
        <?php foreach ($food as $item): ?>
                    <div class="col-sm-3 text-center">
                        <form action="addfavor.php" method="post">
                            <div class="menu-wrap">
                                <img class="menu-img img mb-4"
                                     src="../images/<?php echo $item['avatar'] ?>">
                                <input type="text" value="<?php echo $item['food_id'] ?>" name='food_id' hidden placeholder="">
                        <button type="submit" name="favor" class="btn btn-white btn-outline-white p-2 w-100 mt-4">Order now</button>
                            </div>
                        </form>
                    </div>

        <?php endforeach; ?>
    </div>
</div>

<div class="row slider-text justify-content-center align-items-center">
    <a href="menu.php" class="btn btn-white btn-outline-white w-25 mt-5 mb-5">All Product</a>
</div>

<section class="ftco-about d-md-flex">
    <div class="one-half img" style="background-image: url(../images/about.jpg);"></div>
    <div class="one-half">
        <div class="heading-section">
            <h2 class="mb-4">Welcome to <span class="flaticon-pizza">Pizza</span> A Restaurant</h2>
        </div>
        <div>
            <p style="font-size: 16px">On her way she met a copy. The copy warned the Little Blind Text, that where
                it came from it would have been rewritten a thousand times and everything that was left from its
                origin would be the word "and" and the Little Blind Text should turn around and return to its own,
                safe country. But nothing the copy said could convince her and so it didn’t take long until a few
                insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their
                agency, where they abused her for their.</p>
        </div>
    </div>
</section>

<!--Footer-->
<footer class="ftco-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="mb-4">
                    <h1 class="bread ftco-heading-1"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
                        <small>-
                            Delicous -
                        </small>
                    </h1>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li><a href="#"><span class="icon-twitter"></span></a></li>
                        <li><a href="#"><span class="icon-facebook"></span></a></li>
                        <li><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Shop with us</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">About us</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Terms & Conditions</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Privacy Policy</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">How it works</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">Delivery Area and Free</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Exchange & Refund Policy</a></li>
                        <li><a href="#" class="py-2 d-block color">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Customer Service</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain
                                        View, San Francisco, California, USA</span></li>
                            <li><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></li>
                            <li><span class="icon icon-envelope"></span><span
                                        class="text">info@yourdomain.com</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright &copy; Pizza - Delicious - &copy; All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
<!--End footer-->
<!--Begin script-->
<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/js/bootstrap.min.js"></script>
<!--End script-->
</body>

</html>