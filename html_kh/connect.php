<?php
/**
 * Created by PhpStorm.
 * User: TienNguyen
 * Date: 5/31/2019
 * Time: 9:31 AM
 */

session_start();
$connection = mysqli_connect('localhost', 'root','','pizza');
if (!$connection) {
    die('database failed');
}

$total = 0;
if(isset($_SESSION['cart']) && $_SESSION['cart'] != null) {
    foreach ($_SESSION['cart'] as $list) {
        $total += $list['qty'];
    }
}
$_SESSION['totalqty'] = $total;