<?php
/**
 * Created by PhpStorm.
 * User: TienNguyen
 * Date: 5/27/2019
 * Time: 5:02 PM
 */
require '../html_kh/connect.php';
$id = $_GET['id'];
$_SESSION['cart'][$id]['qty'] -= 1;

if ($_SESSION['cart'][$id]['qty'] <=0) {
    unset($_SESSION['cart'][$id]);
}
$_SESSION['promotion'] = 0;

header('location: ../html_kh/cart.php');