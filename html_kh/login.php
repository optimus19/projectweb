<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 23/05/2019
 * Time: 21:49
 */
require '../html_kh/connect.php';

$email = "";
$password = "";
$errors = array();

if (isset($_POST['submit'])) {
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);

//kiem tra null
    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

//kiem tra thong tin co san hay chua
    $user_check_query = "SELECT email, password, cus_id FROM customer WHERE email='$email' and password = $password LIMIT 1";
    $result = mysqli_query($connection, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    if ($user) { // if user exists
        if ($user['email'] === $email and $user['password'] === $password) {
            header('location: ../html_kh/index.php');
        }
        else array_push($errors, "email hoặc mật khẩu không đúng");
    }
    $_SESSION['email'] = $email;
    $_SESSION['password'] = $password;
    $_SESSION['cus_id'] = $user['cus_id'];
    $_SESSION['promotion'] = 0;
    $_SESSION['promo_id'] = 0;

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/css_login.css">
    <title>Log in - Pizza Delicous</title>
</head>
<body>
    <div class="header-main">
        <div class="header-right">
            <div class="header-left-bottom">
                <form align="center" action="login.php" method="post" style="height: 530px">
                    <h1>Login</h1>
                    <input type="text" name="email" placeholder="Email"/>
                    <input type="password" placeholder="Password" name="password"/>
                    <div class="remember">
                                 <span class="checkbox1">
                                       <label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Remember me</label>
                                 </span>
                        <div class="forgot">
                            <h6><a href="#">Forgot Password?</a></h6>
                        </div>
                    </div>
                    <input type="submit" name="submit" value="Login">
                    <div class="header-left-top">
                        <div class="sign-up"><h2>or</h2></div>
                    </div>
                    <div class="header-social">
                        <a href="#" class="face"><h5>Facebook</h5></a>
                        <a href="#" class="twitt"><h5>Twitter</h5></a>
                    </div>
                    <div class="register">
                        <h6>Don't have account <a href="register.php">Register now!</a></h6>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>