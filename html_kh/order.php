<?php
/**
 * Created by PhpStorm.
 * User: TienNguyen
 * Date: 5/31/2019
 * Time: 9:08 AM
 */

require '../html_kh/connect.php';

$date = date("y/m/d h:i:s");

if (isset($_POST['order'])) {
    $receiver = $_POST['receiver'];
    $address = $_POST['address'];
    $re_phone = $_POST['re_phone'];
    $payment = $_POST['payment'];
}

$cus_id = $_SESSION['cus_id'];
$tongtien = $_SESSION['tongtien'];

$query = "INSERT INTO delivery (name, address, phone)
  			  VALUES('$receiver', '$address', '$re_phone')";
mysqli_query($connection, $query);

$lastest_deli_query = "SELECT max(id) as id FROM delivery";
$result = mysqli_query($connection, $lastest_deli_query);
$del_id = mysqli_fetch_assoc($result);
$deli_id = $del_id['id'];

$total_amount = $_SESSION['total'];
$promo_id = $_SESSION['promo_id'];

$query = "INSERT INTO orders (cus_id, amount, total_amount, promotion_id, payment_method_id, create_date, delivery_id, status)
  			  VALUES('$cus_id', '$tongtien', '$total_amount', '$promo_id', '$payment', '$date', '$deli_id', 5)";
mysqli_query($connection, $query);

$latest_order_id_query = "SELECT max(id) id FROM orders";
$result = mysqli_query($connection, $latest_order_id_query);
$order_id_result = mysqli_fetch_assoc($result);
$latest_order_id = $order_id_result['id'];

foreach ($_SESSION['cart'] as $item):
    $food_id = $item['food_id'] + 0;
    $quantity = $item['qty'];
    $price = $item['food_price'];
    $food_name = $item['food_name'];
    $food_size = $item['size'];
    $total_amount = $price * $quantity;
    $query = "insert into order_details(order_id, food_id, food_size, food_price, food_name, quantity, total_amount) values ('$latest_order_id', '$food_id', '$food_size', '$price', '$food_name', '$quantity', '$total_amount')";
    $query_result = mysqli_query($connection, $query);
endforeach;

echo "<script>alert('Đơn hàng của bạn đã được tiếp nhận, xin hãy tính tục mua hàng!');location.href='index.php'</script>";
session_destroy();