<?php
/**
 * Created by PhpStorm.
 * User: TienNguyen
 * Date: 5/31/2019
 * Time: 3:52 PM
 */

require '../html_kh/connect.php';

if (isset($_POST['promo-code'])) {
     $promo_code = $_POST['code'];
}

$promo_check_query = "SELECT id, percentage FROM promotion WHERE name='$promo_code'";
$result = mysqli_query($connection, $promo_check_query);
$promo = mysqli_fetch_assoc($result);


if ($promo) {
    $_SESSION['promo_id'] = $promo['id'];
    $_SESSION['promotion'] = $_SESSION['tongtien'] * $promo['percentage'];
} else {
    $_SESSION['promotion'] = 0;
    $_SESSION['promo_id'] = 0;
}


header('location: ../html_kh/cart.php');
