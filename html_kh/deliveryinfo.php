<?php
/**
 * Created by PhpStorm.
 * User: MiraLevi
 * Date: 29/05/2019
 * Time: 09:23
 */
require '../html_kh/connect.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="../css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/flaticon.css">
    <title>Pizza Delicous</title>
</head>

<body>
<!--Menu-->
<nav class="navbar navbar-expand-lg bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="about.php"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
            <small>-
                Delicous -
            </small>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="menu.php" class="nav-link">Our Menu</a></li>
                <li class="nav-item"><a href="promotions.php" class="nav-link">Promotion</a></li>
                <li class="nav-item"><a href="about.php" class="nav-link">Abour Us</a></li>
                <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="login.php" class="nav-link"><span
                        class="fa fa-user-circle"></span></a></li>
                <li class="nav-item active"><a href="cart.php" class="nav-link"><span
                        class="fa fa-shopping-cart"></span> Your Cart [<?php echo $_SESSION['totalqty'] ?>]</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--End menu-->
<form action="order.php" class="contact-form" method="post" style="margin-left: 50px; margin-right: 50px">
    <h5 style="margin-top: 30px">1. Delivery Information</h5>
    <div class="row">
        <div class="col-lg-7">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <input name="receiver" type="text" class="form-control" placeholder="The Name of the Recipient">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input name="re_phone" type="number" class="form-control" placeholder="Your Phone">
                    </div>
                </div>
            </div>
            <div class="form-group" style="width: 570px">
                <textarea name="address" id="" cols="10" rows="5" class="form-control" placeholder="Address"></textarea>
            </div>
            <h5 style="margin-top: 30px">2. Payments</h5>
            <div style="margin-top: 20px">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="payment" value="1"
                           checked>
                    <label for="exampleRadios1" style="font-size: 14px; position: relative; top: -7px;">
                        Cash payment on delivery
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="payment" value="1"
                           checked>
                    <label for="exampleRadios1" style="font-size: 14px; position: relative; top: -7px;">
                        Payment by card
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-5 card-right">
            <h5>Your Order</h5>
            <hr>
            <table class="cart-charge" ;>
                <?php foreach ($_SESSION['cart'] as $item): ?>
                    <tr>
                        <td style="display: table-cell; vertical-align: middle; padding-left: 30px;"><?php echo $item['qty'] ?>
                            x
                        </td>
                        <td style="display: table-cell; vertical-align: middle; padding-left: 10px; font-size: 15px"><?php echo $item['food_name'] ?></td>
                        <td style="display: table-cell; vertical-align: middle;padding-left: 10px; padding-right: 30px; text-align: right"><?php echo number_format($item['food_price']) ?>
                            đ
                        </td>
                    </tr>
                <?php endforeach ?>
                <tr style="background: #fac564;">
                    <td colspan="2" style="text-align: center; color: white">Total</td>
                    <td style="text-align: center; color: white"><?php echo number_format($_SESSION['tongtien']) ?> đ</td>
                </tr>
            </table>
            <div>
                <button name="order" type="submit" class="site-btn" style="font-size: 18px; margin-top: 30px;">CONFIRM</button>
            </div>
        </div>
    </div>
</form>
<!--Footer-->
<footer class="ftco-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="mb-4">
                    <h1 class="bread ftco-heading-1"><span class="flaticon-pizza-1 mr-1"></span>Pizza<br>
                        <small>-
                            Delicous -
                        </small>
                    </h1>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li><a href="#"><span class="icon-twitter"></span></a></li>
                        <li><a href="#"><span class="icon-facebook"></span></a></li>
                        <li><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Shop with us</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">About us</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Terms & Conditions</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Privacy Policy</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">How it works</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block color">Delivery Area and Free</span></a></li>
                        <li><a href="#" class="py-2 d-block color">Exchange & Refund Policy</a></li>
                        <li><a href="#" class="py-2 d-block color">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Customer Service</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain
                                        View, San Francisco, California, USA</span></li>
                            <li><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></li>
                            <li><span class="icon icon-envelope"></span><span
                                    class="text">info@yourdomain.com</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright &copy; Pizza - Delicious - &copy; All rights reserved.</p>
            </div>
        </div>
    </div>

</footer>
<!--End footer-->
<!--Begin script-->
<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/js/bootstrap.min.js"></script>
<!--End script-->
</body>
</html>
