<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="assets/css/demo1.css" media="all" />
    <script src="assets/js/Chart.min.js"></script>
    <link rel="icon" href="http://www.thuthuatweb.net/wp-content/themes/HostingSite/favicon.ico" type="image/x-ico"/>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<!-- line chart canvas element -->
        <canvas  id="buyers" width="600" height="400"></canvas>

            <a type="button"  href="http://localhost/projectweb1/trung/thongke.php" class="btn btn-info btn-fill pull-right">Trở về</a>
<script>
	         


	// line chart data
	var buyerData = {
		labels : ["January","February","March","April","May","June"],
		datasets : [
		{
				fillColor : "rgba(172,194,132,0.4)",
				strokeColor : "#ACC26D",
				pointColor : "#fff",
				pointStrokeColor : "#9DB86D",
				data : [203,156,99,251,305,247]
			}
		]
	}

	
	// get line chart canvas
	var buyers = document.getElementById('buyers').getContext('2d');
	// draw line chart
	new Chart(buyers).Line(buyerData);	


</script>
    
</body>
</html>
