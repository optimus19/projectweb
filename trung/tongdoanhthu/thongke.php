<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/Chart.min.js"></script>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/assets/css/demo.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="../trung/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../trung/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="../trung/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="../trung/css/demo.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../trung/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
<style type="text/css">
BODY {
    width: 1000px;
    height: 1000px;
    background: #EEEEEE;
    left: 400px;
    top: 30px;
}

#chart-container {
    width: 500px;
    height: auto;
}
</style>

</head>
<body>
    <div class="sidebar" data-color="purple" data-image="../trung/assets/img/trung.jpg">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text">
                Pizza team
            </a>
        </div>
        <ul class="nav">
            <li>
                <a href="../chart/index.php">
                    <i class="pe-7s-graph"></i>
                    <p>Thông kê món ăn và doanh thu</p>
                </a>
            </li>
            <li>
                <a href="../trung/user.php">
                    <i class="pe-7s-user"></i>
                    <p>Thông tin đăng ký</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#a" aria-expanded="true">
                    <i class="pe-7s-airplay"></i>
                    <p>
                    <p class="dropdown-btn">Quản lý order</p>
                    </p>
                </a>
                <div id="a">
                    <ul class=" nav">
                        <li class="nav-item">
                            <a class="nav-link" href="../html_nv/order_detail.php">
                                <p>Xem tất cả order </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../html_nv/them_order.php">
                                <p>Thêm order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../html_nv/ds_menu.php">
                                <p>Món ăn</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="../html_nv/login.php">
                    <i class="pe-7s-news-paper"></i>
                    <p>Đăng xuất</p>
                </a>
            </li>
        </ul>
    </div>
</div>
        <canvas id="graphCanvas"></canvas>
</body>
    <script>
        $(document).ready(function () {
            showGraph();
        });

        function showGraph()
        {
            {
                $.post("data_chart.php",
                function (data)
                {
                    console.log(data);
                     var name = [];
                    var marks = [];

                    for (var i in data) {
                        name.push(data[i].food_name);
                        marks.push(data[i].total_amount);
                    }

                    var chartdata = {
                        labels: name,
                        datasets: [
                            {
                                label: 'Top 5 pizza bán chạy',
                                backgroundColor: '#FFCCCC',
                                borderColor: '#FFCCCC',
                                hoverBackgroundColor: '#FF6600',
                                hoverBorderColor: '#FF6600',
                                data: marks
                            }
                        ]
                    };

                    var graphTarget = $("#graphCanvas");

                    var barGraph = new Chart(graphTarget, {
                        type: 'bar',
                        data: chartdata
                    });
                });
            }
        }
        </script>
</html>