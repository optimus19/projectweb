<?php
header('Content-Type: application/json');

$conn = mysqli_connect('localhost', 'root','','pizza');

// $sqlQuery = "SELECT food_name,food_id,sum(quantity) FROM order_details group by food_id  DESC  limit 5";
$sqlQuery ="SELECT food_name,food_id,sum(total_amount) as count FROM order_details GROUP BY food_id ORDER BY count DESC";
	
$result = mysqli_query($conn,$sqlQuery);

$data = array();
foreach ($result as $row) {
	$data[] = $row;
}

mysqli_close($conn);

echo json_encode($data);
?>