<?php 
include "database/database.php";
include "database/function.php";
if(isset($_POST['submit'])){
    UpdateInfoUser();
}
 ?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
    
</body>
</html>
<html lang="en" class="nav-open">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>
<div id="nav_position">    
</div>
    <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">User</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left">
                        
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                    </div>
                </div>
            </nav>


        <div class="content" name="dky" method="POST">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">THÔNG TIN ĐĂNG KÝ</h4>
                            </div>
                            <div class="content">
                                <form name="dky" id="dky" action="user.php" method="POST">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Pizza Team (disabled)</label>
                                                <input type="text" class="form-control" disabled  value="Creative Pizza team.">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Tên tài khoản</label>
                                                <input type="text" class="form-control"  name="txtusername" placeholder="AnhTrung">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Số điện thoại</label>
                                                <input type="text" class="form-control" placeholder="0362675736,..." name="txtphone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Họ và tên</label>
                                                <input type="text" class="form-control"  name="txtname" placeholder="Nguyen Van Trung, ..">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Giới tính</label>
                                                <input type="text" class="form-control" placeholder="Nam:1    Nữ:2" name="txtgtinh">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Địa chỉ</label>
                                                <input type="text" class="form-control" placeholder="123 Nguyễn Thị Minh Khai,p.12, quận 1, tp.HCM" name="txtaddress"  >
                                            </div>
                                        </div>
                                    </div>

                                            <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Mật khẩu</label>
                                                <input type="password" class="form-control" placeholder="password" name="txtpassword">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Xác thực mật khẩu</label>
                                                <input type="password" class="form-control" placeholder="confirm-password" name="txtconfirmpassword" >
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" name ="submit"class="btn btn-info btn-fill pull-right">Đăng ký</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
					
                    <div class="col-md-4" id='demo' hidden>
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a href="#">
                                    <img class="avatar border-gray" src="assets/img/faces/backgrounda.jpg" alt="..."/>

                                      <h4 class="title" id="tenngdung"><br />
                                         
                                      </h4>
                                    </a>
                                </div>
                                    <p class="description text-center"  id="fullname"> <br>
                                    <p class="description text-center"  id="tenngdung"> <br>
                                    <p class="description text-center"  id="diachi"> <br>
                                    <p class="description text-center" id="sdt"> <br>
                                </p>
                            </div>
                            <hr>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/chartist.min.js"></script>
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
    <script src="assets/js/demo.js"></script>    
    <!-- chạy thông báo  -->
	<script type="text/javascript" name="notify">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'pe-7s-gift',
            	message: "Bạn đã đăng ký thành công người dùng!</b>"

            },{
                type: 'info',
                timer: 2000
            });

    	});
    </script>

    <!-- navibar -->
    <script>

    $(function () {
        $('#nav_position').load('../html_nv/navigation_bar.html');
    });
</script>
    <!-- chuyển dữ liệu người dùng -->
    <script>
        $(document).ready(function () {
            $("#submit").click(function (e) {
             // hàm xử lý trong nút submit 
             e.preventDefault();
             console.log("ok");
             var dataArray = $("#dky").serializeArray();
             var dataObj = {};
             $(dataArray).each(function (i, field) {
               dataObj[field.name] = field.value;
             });
             console.log(dataArray);
             document.getElementById("tenngdung").innerHTML = dataObj["txtusername"];
            document.getElementById("fullname").innerHTML = dataObj["txtname"];
             document.getElementById("sdt").innerHTML = dataObj["txtphone"];
             document.getElementById("diachi").innerHTML = dataObj["txtaddress"];
             $("#demo").show();
           });
         });
         </script>

</html>